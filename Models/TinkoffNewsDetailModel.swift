//
//  TinkoffNewsDetailModel.swift
//  TinkoffNews
//
//  Created by Daniil Gavrilov on 26.01.17.
//  Copyright © 2017 Daniil Gavrilov. All rights reserved.
//

import UIKit

private struct Constants {
    struct Keys {
        static let text = "content"
        static let modificationDateObject = "lastModificationDate"
        static let timestamp = "milliseconds"
        static let type = "typeId"
    }
}

class TinkoffNewsDetailModel: NSObject {

    // MARK: Public Properties
    
    private(set) var text: String!
    
    private(set) var modificationTimestamp: Int64!
    
    private(set) var loadingTimestamp: TimeInterval!
    
    private(set) var type: String!
    
    // MARK: Init Methods & Superclass Overriders
    
    init(with text: String, modificationTimestamp: Int64, loadingTimestamp: TimeInterval, type: String) {
        self.text = text
        self.modificationTimestamp = modificationTimestamp
        self.loadingTimestamp = loadingTimestamp
        self.type = type
        
        super.init()
    }
    
    class func model(fromDictionary dictionary: [String:Any]) -> TinkoffNewsDetailModel? {
        return TinkoffNewsDetailModel.mapDictionaryToModel(dictionary: dictionary)
    }
    
    override var description: String {
        let namesAndValues = self.propertiesNamesAndValues()
        
        return "TinkoffNewsDetailModel: {\n\(namesAndValues) }"
    }
    
    // MARK: Private Methods
    
    private class func mapDictionaryToModel(dictionary: [String:Any]) -> TinkoffNewsDetailModel? {
        guard let text = dictionary[Constants.Keys.text] as? String else { return nil }
        
        guard let encodedText = text.encodeHTTPString() else { return nil }
        
        guard let modificationnDate = dictionary[Constants.Keys.modificationDateObject] as? [String:Any] else { return nil }
        
        guard let modificationTimestamp = modificationnDate[Constants.Keys.timestamp] as? Int64 else { return nil }
        
        guard let type = dictionary[Constants.Keys.type] as? String else { return nil }
        
        let model = TinkoffNewsDetailModel.init(with: encodedText, modificationTimestamp: modificationTimestamp, loadingTimestamp: Date().timeIntervalSince1970, type: type)
        return model
    }
    
}
