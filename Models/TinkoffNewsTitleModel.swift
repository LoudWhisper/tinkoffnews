//
//  TinkoffNewsTitleModel.swift
//  TinkoffNews
//
//  Created by Daniil Gavrilov on 26.01.17.
//  Copyright © 2017 Daniil Gavrilov. All rights reserved.
//

import UIKit

private struct Constants {
    struct Keys {
        static let newsID = "id"
        static let name = "name"
        static let text = "text"
        static let publicationDateObject = "publicationDate"
        static let timestamp = "milliseconds"
        static let typeID = "bankInfoTypeId"
    }
}

class TinkoffNewsTitleModel: NSObject {
    
    // MARK: Public Properties
    
    private(set) var newsID: Int64!
    
    private(set) var name: String!
    
    private(set) var text: String!
    
    private(set) var publicationTimestamp: Int64!
    
    private(set) var typeID: Int16!
    
    // MARK: Init Methods & Superclass Overriders
    
    init(with newsID: Int64, name: String, text: String, publicationTimestamp: Int64, typeID: Int16) {
        self.newsID = newsID
        self.name = name
        self.text = text
        self.publicationTimestamp = publicationTimestamp
        self.typeID = typeID
        
        super.init()
    }
    
    class func model(fromDictionary dictionary: [String:Any]) -> TinkoffNewsTitleModel? {
        return TinkoffNewsTitleModel.mapDictionaryToModel(dictionary: dictionary)
    }
    
    override var description: String {
        let namesAndValues = self.propertiesNamesAndValues()
        
        return "TinkoffNewsTitleModel: {\n\(namesAndValues) }"
    }
    
    // MARK: Private Methods
    
    private class func mapDictionaryToModel(dictionary: [String:Any]) -> TinkoffNewsTitleModel? {
        guard let newsID = dictionary[Constants.Keys.newsID] as? String else { return nil }
        
        guard let name = dictionary[Constants.Keys.name] as? String else { return nil }
        
        guard let encodedName = name.encodeHTTPString() else { return nil }
        
        guard let text = dictionary[Constants.Keys.text] as? String else { return nil }
        
        guard let encodedText = text.encodeHTTPString() else { return nil }
        
        guard let publicationDate = dictionary[Constants.Keys.publicationDateObject] as? [String:Any] else { return nil }
        
        guard let publicationTimestamp = publicationDate[Constants.Keys.timestamp] as? Int64 else { return nil }
        
        guard let typeID = dictionary[Constants.Keys.typeID] as? Int16 else { return nil }
        
        let model = TinkoffNewsTitleModel.init(with: Int64(newsID)!, name: encodedName, text: encodedText, publicationTimestamp: publicationTimestamp, typeID: Int16(typeID))
        return model
    }

}
