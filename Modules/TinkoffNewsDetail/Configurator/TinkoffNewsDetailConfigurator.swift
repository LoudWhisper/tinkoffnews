//
//  TinkoffNewsDetailModuleConfigurator.swift
//  TinkoffNews
//
//  Created by Daniil Gavrilov on 24/01/2017.
//  Copyright © 2017 Gavrilov Daniil. All rights reserved.
//

import UIKit

class TinkoffNewsDetailModuleConfigurator {
    
    // MARK: Public Methods

    func configureModuleForViewInput<UIViewController>(viewInput: UIViewController) {
        if let viewController = viewInput as? TinkoffNewsDetailViewController {
            configure(viewController: viewController)
        }
    }
    
    // MARK: Private Methods

    private func configure(viewController: TinkoffNewsDetailViewController) {
        let router = TinkoffNewsDetailRouter()
        router.transitionHandler = viewController

        let presenter = TinkoffNewsDetailPresenter()
        presenter.view = viewController
        presenter.router = router

        let interactor = TinkoffNewsDetailInteractor()
        interactor.output = presenter

        presenter.interactor = interactor
        viewController.output = presenter
        viewController.moduleInput = presenter
    }

}
