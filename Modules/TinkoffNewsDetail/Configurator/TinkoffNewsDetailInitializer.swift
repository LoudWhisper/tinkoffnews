//
//  TinkoffNewsDetailModuleInitializer.swift
//  TinkoffNews
//
//  Created by Daniil Gavrilov on 24/01/2017.
//  Copyright © 2017 Gavrilov Daniil. All rights reserved.
//

import UIKit

class TinkoffNewsDetailModuleInitializer: NSObject {

    // MARK: Interface Builder Properties
    
    @IBOutlet weak var tinkoffNewsDetailViewController: TinkoffNewsDetailViewController!

    // MARK: Init Methods & Superclass Overriders
    
    override func awakeFromNib() {
        let configurator = TinkoffNewsDetailModuleConfigurator()
        configurator.configureModuleForViewInput(viewInput: self.tinkoffNewsDetailViewController)
    }

}
