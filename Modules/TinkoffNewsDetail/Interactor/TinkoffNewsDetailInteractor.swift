//
//  TinkoffNewsDetailInteractor.swift
//  TinkoffNews
//
//  Created by Daniil Gavrilov on 24/01/2017.
//  Copyright © 2017 Gavrilov Daniil. All rights reserved.
//

import Foundation

class TinkoffNewsDetailInteractor: TinkoffNewsDetailInteractorInput {

    // MARK: Public Properties
    
    weak var output: TinkoffNewsDetailInteractorOutput!
    
    var newsDataService: TinkoffNewsDataService!
    
    var newsNetworkService: TinkoffNewsNetworkService!
    
    // MARK: Private Methods
    
    private func saveNewsDetail(newsID: Int64, detail: [String:Any]) {
        self.newsDataService.saveNewsDetail(newsID: newsID, object: detail) { [weak self] (model) -> (Void) in
            self?.output.didLoad(newsDetailModel: model)
        }
    }
    
    private func loadNewsDetail(newsID: Int64) {
        self.newsNetworkService.getNewsDetail(newsID: newsID) { (response, error, isNotReachable, isTimedOut) -> (Void) in
            if let responseDictionary = response as? [String:Any] {
                self.saveNewsDetail(newsID: newsID, detail: responseDictionary)
            } else if error != nil {
                self.showError(error: error!)
            }
        }
    }
    
    private func loadSavedNewsDetail(newsID: Int64) {
        self.newsDataService.savedNewsDetail(newsID: newsID) { [weak self] (model) -> (Void) in
            if model != nil {
                self?.output.didLoad(newsDetailModel: model)
                self?.checkLoadingDateAndReloadIfNeeded(newsID: newsID, model: model!)
            } else {
                self?.loadNewsDetail(newsID: newsID)
            }
        }
    }
    
    private func checkLoadingDateAndReloadIfNeeded(newsID: Int64, model: TinkoffNewsDetailModel) {
        let currentTimestamp = Date().timeIntervalSince1970
        let difference = currentTimestamp - model.loadingTimestamp
        
        let differenceInHours = difference / 3600
        if differenceInHours > 1 {
            self.loadNewsDetail(newsID: newsID)
        }
    }
    
    private func showError(error: String) {
        performBlockInMainQueue {
            self.output.didReceiveError(error: error)
        }
    }
    
    // MARK: Protocols Implementation
    
    // MARK: TinkoffNewsDetailInteractorInput
    
    func set(newsDataService: TinkoffNewsDataService) {
        self.newsDataService = newsDataService
    }
    
    func set(newsNetworkService: TinkoffNewsNetworkService) {
        self.newsNetworkService = newsNetworkService
    }
    
    func getSavedNewsDetailModelAndLoadIfNeeded(newsID: Int64) {
        self.loadSavedNewsDetail(newsID: newsID)
    }

}
