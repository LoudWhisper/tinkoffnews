//
//  TinkoffNewsDetailInteractorInput.swift
//  TinkoffNews
//
//  Created by Daniil Gavrilov on 24/01/2017.
//  Copyright © 2017 Gavrilov Daniil. All rights reserved.
//

import Foundation

protocol TinkoffNewsDetailInteractorInput {
    
    func set(newsDataService: TinkoffNewsDataService)
    
    func set(newsNetworkService: TinkoffNewsNetworkService)
    
    func getSavedNewsDetailModelAndLoadIfNeeded(newsID: Int64)

}
