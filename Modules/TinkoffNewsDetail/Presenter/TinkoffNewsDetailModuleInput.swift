//
//  TinkoffNewsDetailModuleInput.swift
//  TinkoffNews
//
//  Created by Daniil Gavrilov on 24/01/2017.
//  Copyright © 2017 Gavrilov Daniil. All rights reserved.
//

import ViperMcFlurry

protocol TinkoffNewsDetailModuleInput: class, RamblerViperModuleInput {
    
    func addNewsTitleModel(newsTitleModel: TinkoffNewsTitleModel)
    
    func addServices(newsDataService: TinkoffNewsDataService, newsNetworkService: TinkoffNewsNetworkService)

}
