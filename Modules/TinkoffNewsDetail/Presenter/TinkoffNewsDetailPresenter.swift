//
//  TinkoffNewsDetailPresenter.swift
//  TinkoffNews
//
//  Created by Daniil Gavrilov on 24/01/2017.
//  Copyright © 2017 Gavrilov Daniil. All rights reserved.
//

import Foundation

class TinkoffNewsDetailPresenter: NSObject, TinkoffNewsDetailModuleInput, TinkoffNewsDetailViewOutput, TinkoffNewsDetailInteractorOutput {

    // MARK: Public Properties
    
    weak var view: TinkoffNewsDetailViewInput!
    
    var interactor: TinkoffNewsDetailInteractorInput!
    
    var router: TinkoffNewsDetailRouterInput!
    
    // MARK: Private Properties
    
    private var newsTitleModel: TinkoffNewsTitleModel!
    
    // MARK: Protocols Implemetation
    
    // MARK: TinkoffNewsDetailModuleInput
    
    func addNewsTitleModel(newsTitleModel: TinkoffNewsTitleModel) {
        self.newsTitleModel = newsTitleModel
    }
    
    func addServices(newsDataService: TinkoffNewsDataService, newsNetworkService: TinkoffNewsNetworkService) {
        self.interactor.set(newsDataService: newsDataService)
        self.interactor.set(newsNetworkService: newsNetworkService)
    }
    
    // MARK: TinkoffNewsDetailViewOutput
    
    func viewIsReady() {
        self.view.setupInitialState(with: String(self.newsTitleModel.text))
        
        self.interactor.getSavedNewsDetailModelAndLoadIfNeeded(newsID: self.newsTitleModel.newsID)
    }
    
    func backButtonClicked() {
        self.router.closeModule()
    }
    
    // MARK: TinkoffNewsDetailInteractorOutput
    
    func didLoad(newsDetailModel model: TinkoffNewsDetailModel?) {
        self.view.showNewsTitles(detailModel: model)
    }
    
    func didReceiveError(error: String) {
        self.view.showError(error: error)
    }
    
}
