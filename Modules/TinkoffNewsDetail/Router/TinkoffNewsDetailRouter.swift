//
//  TinkoffNewsDetailRouter.swift
//  TinkoffNews
//
//  Created by Daniil Gavrilov on 24/01/2017.
//  Copyright © 2017 Gavrilov Daniil. All rights reserved.
//

import Foundation
import ViperMcFlurry

class TinkoffNewsDetailRouter: TinkoffNewsDetailRouterInput {
    
    // MARK: Public Properties
    
    weak var transitionHandler: RamblerViperModuleTransitionHandlerProtocol!
    
    // MARK: Protocols Implementation
    
    // MARK: TinkoffNewsDetailRouterInput
    
    func closeModule() {
        self.transitionHandler.closeCurrentModule!(true)
    }
    
}
