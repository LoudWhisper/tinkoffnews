//
//  TinkoffNewsDetailRouterInput.swift
//  TinkoffNews
//
//  Created by Daniil Gavrilov on 24/01/2017.
//  Copyright © 2017 Gavrilov Daniil. All rights reserved.
//

import Foundation

protocol TinkoffNewsDetailRouterInput {
    
    func closeModule()

}
