//
//  TinkoffNewsDetailViewController.swift
//  TinkoffNews
//
//  Created by Daniil Gavrilov on 24/01/2017.
//  Copyright © 2017 Gavrilov Daniil. All rights reserved.
//

import UIKit

class TinkoffNewsDetailViewController: UIViewController, TinkoffNewsDetailViewInput {
    
    // MARK: Interface Builder Properties
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var textView: UITextView!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!

    // MARK: Public Properties
    
    var output: TinkoffNewsDetailViewOutput!
    
    // MARK: Private Properties
    
    private var viewModelReceived = false

    // MARK: Init Methods & Superclass Overriders
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.textView.textContainerInset = UIEdgeInsetsMake(16.0, 16.0, 16.0, 16.0)
        
        self.output.viewIsReady()
    }
    
    // MARK: Controls Actions
    
    @IBAction func backButtonAction(sender: UIButton) {
        self.output.backButtonClicked()
    }
    
    // MARK: Protocols Implementation
    
    // MARK: TinkoffNewsListViewInput
    
    func setupInitialState(with title: String) {
        self.titleLabel.text = title
        self.textView.text = nil
    }
    
    func showNewsTitles(detailModel model: TinkoffNewsDetailModel?) {
        if model != nil {
            self.viewModelReceived = true
            self.textView.text = model?.text
            self.activityIndicator.stopAnimating()
        }
    }
    
    func showError(error: String) {
        self.presentErrorAlert(with: error, cancelButtonAction: {
            if !self.viewModelReceived {
                self.output.backButtonClicked()
            }
        }, completion: nil)
    }
    
}
