//
//  TinkoffNewsDetailViewInput.swift
//  TinkoffNews
//
//  Created by Daniil Gavrilov on 24/01/2017.
//  Copyright © 2017 Gavrilov Daniil. All rights reserved.
//

protocol TinkoffNewsDetailViewInput: class {

    func setupInitialState(with title: String)
    
    func showNewsTitles(detailModel model: TinkoffNewsDetailModel?)
    
    func showError(error: String)
    
}
