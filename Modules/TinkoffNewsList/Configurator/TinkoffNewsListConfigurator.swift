//
//  TinkoffNewsListModuleConfigurator.swift
//  TinkoffNews
//
//  Created by Daniil Gavrilov on 24/01/2017.
//  Copyright © 2017 Gavrilov Daniil. All rights reserved.
//

import UIKit

class TinkoffNewsListModuleConfigurator {
    
    // MARK: Public Methods

    func configureModuleForViewInput<UIViewController>(viewInput: UIViewController) {
        if let viewController = viewInput as? TinkoffNewsListViewController {
            configure(viewController: viewController)
        }
    }
    
    // MARK: Private Methods

    private func configure(viewController: TinkoffNewsListViewController) {
        let router = TinkoffNewsListRouter()
        router.transitionHandler = viewController

        let presenter = TinkoffNewsListPresenter()
        presenter.view = viewController
        presenter.router = router

        let interactor = TinkoffNewsListInteractor()
        interactor.output = presenter
        interactor.newsDataService = TinkoffNewsDataService()
        interactor.newsNetworkService = TinkoffNewsNetworkService()

        presenter.interactor = interactor
        viewController.output = presenter
    }

}
