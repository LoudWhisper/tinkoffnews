//
//  TinkoffNewsListModuleInitializer.swift
//  TinkoffNews
//
//  Created by Daniil Gavrilov on 24/01/2017.
//  Copyright © 2017 Gavrilov Daniil. All rights reserved.
//

import UIKit

class TinkoffNewsListModuleInitializer: NSObject {

    // MARK: Interface Builder Properties
    
    @IBOutlet weak var tinkoffNewsListViewController: TinkoffNewsListViewController!

    // MARK: Init Methods & Superclass Overriders
    
    override func awakeFromNib() {
        let configurator = TinkoffNewsListModuleConfigurator()
        configurator.configureModuleForViewInput(viewInput: self.tinkoffNewsListViewController)
    }

}
