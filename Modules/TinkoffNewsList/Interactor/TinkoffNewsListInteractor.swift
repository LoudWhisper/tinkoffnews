//
//  TinkoffNewsListInteractor.swift
//  TinkoffNews
//
//  Created by Daniil Gavrilov on 24/01/2017.
//  Copyright © 2017 Gavrilov Daniil. All rights reserved.
//

import Foundation

class TinkoffNewsListInteractor: TinkoffNewsListInteractorInput {
    
    // MARK: Public Properties

    weak var output: TinkoffNewsListInteractorOutput!
    
    var newsDataService: TinkoffNewsDataService!
    
    var newsNetworkService: TinkoffNewsNetworkService!
    
    // MARK: Private Methods
    
    private func saveNewsTitles(titles: Array<Any>) {
        self.newsDataService.saveNewsTitles(objects: titles) { [weak self] (models) -> (Void) in
            self?.output.didLoadNew(newsTitlesModels: models)
        }
    }
    
    private func loadNewsTitles() {
        self.newsNetworkService.getNewsList { (response, error, isNotReachable, isTimedOut) -> (Void) in
            if let responseArray = response as? Array<Any> {
                self.saveNewsTitles(titles: responseArray)
            } else if error != nil {
                self.showError(error: error!)
            }
        }
    }
    
    private func loadSavedNewsTitles() {
        self.newsDataService.savedNewsTitles { [weak self] (models) -> (Void) in
            self?.output.didLoadSaved(newsTitlesModels: models)
        }
    }
    
    private func showError(error: String) {
        performBlockInMainQueue {
            self.output.didReceiveError(error: error)
        }
    }
    
    // MARK: Protocols Implemetation
    
    // MARK: TinkoffNewsListInteractorInput
    
    func getSharedServices() -> (TinkoffNewsDataService, TinkoffNewsNetworkService) {
        return (self.newsDataService, self.newsNetworkService)
    }
    
    func getSavedNewsTitlesModels() {
        self.loadSavedNewsTitles()
    }
    
    func loadNewsTitlesModels() {
        self.loadNewsTitles()
    }

}
