//
//  TinkoffNewsListInteractorInput.swift
//  TinkoffNews
//
//  Created by Daniil Gavrilov on 24/01/2017.
//  Copyright © 2017 Gavrilov Daniil. All rights reserved.
//

import Foundation

protocol TinkoffNewsListInteractorInput {
    
    func getSharedServices() -> (TinkoffNewsDataService, TinkoffNewsNetworkService)
    
    func getSavedNewsTitlesModels()
    
    func loadNewsTitlesModels()

}
