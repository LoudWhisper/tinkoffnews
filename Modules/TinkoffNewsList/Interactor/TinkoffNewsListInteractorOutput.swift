//
//  TinkoffNewsListInteractorOutput.swift
//  TinkoffNews
//
//  Created by Daniil Gavrilov on 24/01/2017.
//  Copyright © 2017 Gavrilov Daniil. All rights reserved.
//

import Foundation

protocol TinkoffNewsListInteractorOutput: class {
    
    func didLoadSaved(newsTitlesModels models: [TinkoffNewsTitleModel])
    
    func didLoadNew(newsTitlesModels models: [TinkoffNewsTitleModel])
    
    func didReceiveError(error: String)

}
