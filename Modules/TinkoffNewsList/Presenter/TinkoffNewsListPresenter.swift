//
//  TinkoffNewsListPresenter.swift
//  TinkoffNews
//
//  Created by Daniil Gavrilov on 24/01/2017.
//  Copyright © 2017 Gavrilov Daniil. All rights reserved.
//

import Foundation

class TinkoffNewsListPresenter: NSObject, TinkoffNewsListModuleInput, TinkoffNewsListViewOutput, TinkoffNewsListInteractorOutput {

    // MARK: Public Properties
    
    weak var view: TinkoffNewsListViewInput!
    
    var interactor: TinkoffNewsListInteractorInput!
    
    var router: TinkoffNewsListRouterInput!
    
    // MARK: Private Properties
    
    private var isLoading: Bool = false
    
    private var newsTitlesModels: [TinkoffNewsTitleModel]?
    
    // MARK: Protocols Implemetation
    
    // MARK: TinkoffNewsListViewOutput
    
    func viewIsReady() {
        self.view.setupInitialState()
        
        self.interactor.getSavedNewsTitlesModels()
    }
    
    func newsDetailClicked(with newsTitleModel: TinkoffNewsTitleModel) {
        let (newsDataService, newsNetworkService) = self.interactor.getSharedServices()
        
        self.router.openTinkoffNewsDetailViewController(newsTitleModel: newsTitleModel, newsDataService: newsDataService, newsNetworkService: newsNetworkService)
    }
    
    func newsListReloading() {
        self.isLoading = true
        
        self.interactor.loadNewsTitlesModels()
    }
    
    // MARK : TinkoffNewsListInteractorOutput
    
    func didLoadSaved(newsTitlesModels models: [TinkoffNewsTitleModel]) {
        self.isLoading = models.count == 0
        self.newsTitlesModels = models
        
        self.view.showNewsTitles(titlesModels: models, isLoading: self.isLoading)
        
        if models.count == 0 {
            self.interactor.loadNewsTitlesModels()
        }
    }
    
    func didLoadNew(newsTitlesModels models: [TinkoffNewsTitleModel]) {
        self.isLoading = false
        self.newsTitlesModels = models
        
        self.view.showNewsTitles(titlesModels: models, isLoading: self.isLoading)
    }
    
    func didReceiveError(error: String) {
        self.isLoading = false
        
        self.view.showError(error: error) {
            self.view.showNewsTitles(titlesModels: self.newsTitlesModels, isLoading: self.isLoading)
        }
    }
    
}
