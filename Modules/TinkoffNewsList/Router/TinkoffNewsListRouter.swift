//
//  TinkoffNewsListRouter.swift
//  TinkoffNews
//
//  Created by Daniil Gavrilov on 24/01/2017.
//  Copyright © 2017 Gavrilov Daniil. All rights reserved.
//

import Foundation
import ViperMcFlurry

private struct Segues {
    static let toNewsDetail = "TinkoffNewsDetailViewController"
}

class TinkoffNewsListRouter: TinkoffNewsListRouterInput {
    
    // MARK: Public Properties
    
    weak var transitionHandler: RamblerViperModuleTransitionHandlerProtocol!
    
    // MARK: Protocols Implementation
    
    // MARK: TinkoffNewsListRouterInput
    
    func openTinkoffNewsDetailViewController(newsTitleModel: TinkoffNewsTitleModel, newsDataService: TinkoffNewsDataService, newsNetworkService: TinkoffNewsNetworkService) {
        self.transitionHandler.openModule!(usingSegue: Segues.toNewsDetail).thenChain { (input) -> RamblerViperModuleOutput? in
            if let newsDetailInput = input as? TinkoffNewsDetailModuleInput {
                newsDetailInput.addNewsTitleModel(newsTitleModel: newsTitleModel)
                newsDetailInput.addServices(newsDataService: newsDataService, newsNetworkService: newsNetworkService)
            }
            return nil
        }
    }

}
