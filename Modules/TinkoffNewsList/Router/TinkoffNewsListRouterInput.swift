//
//  TinkoffNewsListRouterInput.swift
//  TinkoffNews
//
//  Created by Daniil Gavrilov on 24/01/2017.
//  Copyright © 2017 Gavrilov Daniil. All rights reserved.
//

import Foundation

protocol TinkoffNewsListRouterInput {
    
    func openTinkoffNewsDetailViewController(newsTitleModel: TinkoffNewsTitleModel, newsDataService: TinkoffNewsDataService, newsNetworkService: TinkoffNewsNetworkService)

}
