//
//  TinkoffNewsListViewController.swift
//  TinkoffNews
//
//  Created by Daniil Gavrilov on 24/01/2017.
//  Copyright © 2017 Gavrilov Daniil. All rights reserved.
//

import UIKit
import TableKit
import ViperMcFlurry

class TinkoffNewsListViewController: UIViewController, TinkoffNewsListViewInput {
    
    // MARK: Interface Builder Properties
    
    @IBOutlet weak var tableView: UITableView!

    // MARK: Public Properties
    
    var output: TinkoffNewsListViewOutput!
    
    // MARK: Private Properties
    
    private var tableDirector: TableDirector!
    
    private var refreshControl: UIRefreshControl!
    
    private var isLoading: Bool = false

    // MARK: Init Methods & Superclass Overriders
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupTableViewRefreshing()
        self.output.viewIsReady()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.navigationController?.interactivePopGestureRecognizer?.delegate = nil
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let configurationHolder = segue.destination
        
        if let configurationBlock = sender as? RamblerViperOpenModulePromise {
            configurationBlock.moduleInput = configurationHolder.moduleInput
        }
    }
    
    // MARK: Private Methods
    
    // MARK: Default Setups
    
    private func setupTableViewRefreshing() {
        self.refreshControl = UIRefreshControl()
        self.refreshControl.tintColor = .white
        self.refreshControl.addTarget(self, action: #selector(refreshControl(sender:)), for: .valueChanged)
    }
    
    // MARK: Support Methods
    
    private func setRefreshControlState(isLoading: Bool) {
        if !isLoading {
            if self.refreshControl.superview == nil {
                self.tableView.addSubview(self.refreshControl)
            } else {
                self.refreshControl.endRefreshing()
            }
        }
    }
    
    private func refreshTableView(titlesModels models: [TinkoffNewsTitleModel]?, isLoading: Bool) {
        self.isLoading = isLoading
        
        self.setRefreshControlState(isLoading: isLoading)
        
        let action = TableRowAction<TinkoffNewsListTableViewCell>(.click) { [weak self] (options) in
            let model = options.item
            self?.showDetailView(with: model)
        }
        
        var rows: [Row] = []
        if models != nil {
            for model in models! {
                let row = TableRow<TinkoffNewsListTableViewCell>(item: model, actions: [action])
                rows.append(row)
            }
        }
        
        if rows.count == 0 {
            let row = TableRow<TinkoffNewsEmptyTableViewCell>(item: isLoading ? .loading : .empty)
            rows.append(row)
        }
        
        let section = TableSection(rows: rows)
        section.headerHeight = 1
        section.footerHeight = 1
        section.headerView = UIView.init(frame: CGRect.zero)
        section.footerView = UIView.init(frame: CGRect.zero)
        
        self.tableDirector.clear()
        self.tableDirector += section
        self.tableDirector.reload()
    }
    
    // MARK: Controls Actions
    
    private func showDetailView(with model: TinkoffNewsTitleModel) {
        self.output.newsDetailClicked(with: model)
    }
    
    @objc private func refreshControl(sender: UIRefreshControl) {
        if !self.isLoading {
            self.output.newsListReloading()
        }
    }

    // MARK: Protocols Implementation
    
    // MARK: TinkoffNewsListViewInput
    
    func setupInitialState() {
        self.tableDirector = TableDirector(tableView: self.tableView)
        
        if self.tableDirector.isEmpty {
            self.refreshTableView(titlesModels: nil, isLoading: true)
        }
    }
    
    func showNewsTitles(titlesModels models: [TinkoffNewsTitleModel]?, isLoading: Bool) {
        self.refreshTableView(titlesModels: models, isLoading: isLoading)
    }
    
    func showError(error: String, completion: ((Void) -> Void)?) {
        self.presentErrorAlert(with: error, cancelButtonAction: nil, completion: completion)
    }
    
}
