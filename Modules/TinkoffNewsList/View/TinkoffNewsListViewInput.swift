//
//  TinkoffNewsListViewInput.swift
//  TinkoffNews
//
//  Created by Daniil Gavrilov on 24/01/2017.
//  Copyright © 2017 Gavrilov Daniil. All rights reserved.
//

protocol TinkoffNewsListViewInput: class {

    func setupInitialState()
    
    func showNewsTitles(titlesModels models: [TinkoffNewsTitleModel]?, isLoading: Bool)
    
    func showError(error: String, completion: ((Void) -> Void)?)
    
}
