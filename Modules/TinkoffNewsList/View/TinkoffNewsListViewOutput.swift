//
//  TinkoffNewsListViewOutput.swift
//  TinkoffNews
//
//  Created by Daniil Gavrilov on 24/01/2017.
//  Copyright © 2017 Gavrilov Daniil. All rights reserved.
//

protocol TinkoffNewsListViewOutput {

    func viewIsReady()
    
    func newsDetailClicked(with newsTitleModel: TinkoffNewsTitleModel)
    
    func newsListReloading()
    
}
