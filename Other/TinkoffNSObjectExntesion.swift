//
//  TinkoffNSObjectExntesion.swift
//  TinkoffNews
//
//  Created by Daniil Gavrilov on 29.01.17.
//  Copyright © 2017 Daniil Gavrilov. All rights reserved.
//

import Foundation

extension NSObject {
    
    func propertiesNamesAndValues() -> [String:Any] {
        var namesAndValues: [String:Any] = [:]
        
        let mirror = Mirror(reflecting: self)
        for child in mirror.children {
            guard let key = child.label else { continue }
            let value = child.value
            
            guard let result = self.unwrap(value) else { continue }
            
            namesAndValues[key] = result
        }
        
        return namesAndValues
    }
    
    private func unwrap(_ subject: Any) -> Any? {
        var value: Any?
        let mirrored = Mirror(reflecting:subject)
        if mirrored.displayStyle != .optional {
            value = subject
        } else if let firstChild = mirrored.children.first {
            value = firstChild.value
        }
        return value
    }
    
}
