//
//  TinkoffStringExtension.swift
//  TinkoffNews
//
//  Created by Daniil Gavrilov on 29.01.17.
//  Copyright © 2017 Daniil Gavrilov. All rights reserved.
//

import UIKit

extension String {
    
    func encodeHTTPString() -> String? {
        guard let textData = self.data(using: .utf8) else { return nil }
        
        guard let attributedTet = try? NSAttributedString(data: textData, options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: String.Encoding.utf8.rawValue], documentAttributes: nil) else { return nil }
        
        return attributedTet.string
    }
    
}
