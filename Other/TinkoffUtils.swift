//
//  TinkoffUtils.swift
//  TinkoffNews
//
//  Created by Daniil Gavrilov on 29.01.17.
//  Copyright © 2017 Daniil Gavrilov. All rights reserved.
//

import Foundation

func performBlockInMainQueue(block: @escaping () -> (Void)) {
    if Thread.isMainThread {
        block()
    } else {
        DispatchQueue.main.async {
            block()
        }
    }
}
