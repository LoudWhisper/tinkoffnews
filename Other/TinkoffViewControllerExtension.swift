//
//  TinkoffViewControllerExtension.swift
//  TinkoffNews
//
//  Created by Daniil Gavrilov on 29.01.17.
//  Copyright © 2017 Daniil Gavrilov. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func presentErrorAlert(with text: String?, cancelButtonAction: ((Void) -> Void)?, completion: ((Void) -> Void)?) {
        let title = "Ошибка"
        let message = text?.characters.count == 0 ? "Неизвестна ошибка. Пожалуйста, свяжитесь со службой поддержки." : text
        
        let alertController = UIAlertController.init(title: title, message: message, preferredStyle: .alert)
        let cancelAction = UIAlertAction.init(title: "Хорошо", style: .cancel) { (action) in
            cancelButtonAction?()
        }
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: completion)
    }
    
}
