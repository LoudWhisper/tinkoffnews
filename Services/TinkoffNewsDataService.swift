//
//  TinkoffNewsDataService.swift
//  TinkoffNews
//
//  Created by Daniil Gavrilov on 26.01.17.
//  Copyright © 2017 Daniil Gavrilov. All rights reserved.
//

import UIKit
import CoreData

class TinkoffNewsDataService {
    
    // MARK: Private Properties
    
    private let titlesModelsQueue = DispatchQueue(label: "NewsDataService_titlesModelsQueue")
    
    private let detailModelsQueue = DispatchQueue(label: "NewsDataService_detailModelsQueue")
    
    private var persistentContainer: NSPersistentContainer!
    
    private var writerContext: NSManagedObjectContext!
    
    // MARK: Init Methods & Superclass Overriders
    
    init() {
        self.setupCoreDataStack()
    }
    
    // MARK: Public Methods
    
    // MARK: TinkoffNewsTitleModel Methods
    
    func savedNewsTitles(completion: @escaping (_ models: [TinkoffNewsTitleModel]) -> (Void)) {
        self.coreDataLoadNewsTitlesModels { (models) -> (Void) in
            completion(models)
        }
    }
    
    func saveNewsTitles(objects: Array<Any>, completion: @escaping (_ models: [TinkoffNewsTitleModel]) -> (Void)) {
        self.titlesModelsQueue.async {
            var models: [TinkoffNewsTitleModel] = []
            
            for object in objects {
                if let dictionaryObject = object as? [String:Any] {
                    if let model = TinkoffNewsTitleModel.model(fromDictionary: dictionaryObject) {
                        models.append(model)
                    }
                }
            }
            
            self.coreDataSaveNewsTitlesModels(models: models)
            
            models.sort(by: { $0.publicationTimestamp > $1.publicationTimestamp })
            
            performBlockInMainQueue {
                completion(models)
            }
        }
    }
    
    // MARK: TinkoffNewsDetailModel Methods
    
    func savedNewsDetail(newsID: Int64, completion: @escaping (_ model: TinkoffNewsDetailModel?) -> (Void)) {
        self.coreDataLoadNewsDetailModel(newsID: newsID) { (model) -> (Void) in
            completion(model)
        }
    }
    
    func saveNewsDetail(newsID: Int64, object: [String:Any], completion: @escaping (_ model: TinkoffNewsDetailModel?) -> (Void)) {
        self.detailModelsQueue.async {
            let model = TinkoffNewsDetailModel.model(fromDictionary: object)
            
            if model != nil {
                self.coreDataSaveNewsDetail(newsID: newsID, model: model!)
            }
            
            performBlockInMainQueue {
                completion(model)
            }
        }
    }
    
    // MARK: Private Methods
    
    // MARK: Core Data
    
    private func setupCoreDataStack() {
        self.persistentContainer = NSPersistentContainer(name: "TinkoffNews")
        self.persistentContainer.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        
        self.writerContext = NSManagedObjectContext.init(concurrencyType: .privateQueueConcurrencyType)
        self.writerContext.persistentStoreCoordinator = self.persistentContainer.persistentStoreCoordinator
    }
    
    private func coreDataSaveNewsDetail(newsID: Int64, model: TinkoffNewsDetailModel) {
        let context = NSManagedObjectContext.init(concurrencyType: .privateQueueConcurrencyType)
        context.parent = self.writerContext
        
        context.perform {
            let newsListPredicate = NSPredicate.init(format: "newsID == %d", newsID)
            
            let newsListFetchRequest = NSFetchRequest<NSManagedObject>.init(entityName: "NewsList")
            newsListFetchRequest.predicate = newsListPredicate
        
            guard let newsListFetchRequestResult = try? context.fetch(newsListFetchRequest) else { return }
            
            if let newsListCoreDataModel = newsListFetchRequestResult.first {
                let newsDetailPredicate = NSPredicate.init(format: "title.newsID == %d", newsID)
                
                let newsDetailFetchRequest = NSFetchRequest<NSManagedObject>.init(entityName: "NewsDetail")
                newsDetailFetchRequest.predicate = newsDetailPredicate
                
                guard let newsDetailFetchRequestResult = try? context.fetch(newsDetailFetchRequest) else { return }
                
                var coreDataModel: NSManagedObject?
                if let fetchedCoreDataModel = newsDetailFetchRequestResult.first {
                    coreDataModel = fetchedCoreDataModel
                    
                    let currentModificationTimestamp = coreDataModel!.value(forKey: "modificationTimestamp") as! Int64
                    if model.modificationTimestamp >= currentModificationTimestamp {
                        return
                    }
                } else {
                    guard let entity = NSEntityDescription.entity(forEntityName: "NewsDetail", in: context) else { return }
                    
                    coreDataModel = NSManagedObject(entity: entity, insertInto: context)
                }
                
                coreDataModel!.setValue(model.text, forKeyPath: "text")
                coreDataModel!.setValue(model.modificationTimestamp, forKeyPath: "modificationTimestamp")
                coreDataModel!.setValue(model.loadingTimestamp, forKeyPath: "loadingTimestamp")
                coreDataModel!.setValue(model.type, forKeyPath: "type")
                
                newsListCoreDataModel.setValue(coreDataModel, forKey: "detail")
                
                self.coreDataSave(context: context)
                
                self.writerContext.perform {
                    self.coreDataSave(context: self.writerContext)
                }
            }
        }
    }
    
    private func coreDataSaveNewsTitlesModels(models: [TinkoffNewsTitleModel]) {
        let context = NSManagedObjectContext.init(concurrencyType: .privateQueueConcurrencyType)
        context.parent = self.writerContext
        
        context.perform {
            let deleteFetchRequest = NSFetchRequest<NSFetchRequestResult>.init(entityName: "NewsList")
            let deleteBatchRequest = NSBatchDeleteRequest(fetchRequest: deleteFetchRequest)
            
            do {
                try context.execute(deleteBatchRequest)
            } catch {
                let nserror = error as NSError
                print("Unresolved error \(nserror), \(nserror.userInfo)")
            }
            
            guard let entity = NSEntityDescription.entity(forEntityName: "NewsList", in: context) else { return }
            
            for model in models {
                let coreDataModel = NSManagedObject(entity: entity, insertInto: context)
                coreDataModel.setValue(model.newsID, forKeyPath: "newsID")
                coreDataModel.setValue(model.name, forKeyPath: "name")
                coreDataModel.setValue(model.text, forKeyPath: "text")
                coreDataModel.setValue(model.publicationTimestamp, forKeyPath: "publicationTimestamp")
                coreDataModel.setValue(model.typeID, forKeyPath: "typeID")
            }
            
            self.coreDataSave(context: context)
            
            self.writerContext.perform {
                self.coreDataSave(context: self.writerContext)
            }
        }
    }
    
    private func coreDataLoadNewsDetailModel(newsID: Int64, completion: @escaping (_ model: TinkoffNewsDetailModel?) -> (Void)) {
        let context = NSManagedObjectContext.init(concurrencyType: .privateQueueConcurrencyType)
        context.parent = self.writerContext
        
        context.perform {
            let predicate = NSPredicate.init(format: "title.newsID == %d", newsID)
            
            let fetchRequest = NSFetchRequest<NSManagedObject>.init(entityName: "NewsDetail")
            fetchRequest.predicate = predicate
            
            var model: TinkoffNewsDetailModel?
            if let fetchRequestResult = try? context.fetch(fetchRequest) {
                if let coreDataModel = fetchRequestResult.first {
                    let text = coreDataModel.value(forKey: "text") as! String
                    let modificationTimestamp = coreDataModel.value(forKey: "modificationTimestamp") as! Int64
                    let loadingTimestamp = coreDataModel.value(forKey: "loadingTimestamp") as! TimeInterval
                    let type = coreDataModel.value(forKey: "type") as! String
                    
                    model = TinkoffNewsDetailModel.init(with: text, modificationTimestamp: modificationTimestamp, loadingTimestamp: loadingTimestamp, type: type)
                }
            }
        
            performBlockInMainQueue {
                completion(model)
            }
        }
    }
    
    private func coreDataLoadNewsTitlesModels(completion: @escaping (_ models: [TinkoffNewsTitleModel]) -> (Void)) {
        let context = NSManagedObjectContext.init(concurrencyType: .privateQueueConcurrencyType)
        context.parent = self.writerContext
        
        context.perform {
            let sortDescriptor = NSSortDescriptor.init(key: "publicationTimestamp", ascending: false)
            
            let fetchRequest = NSFetchRequest<NSManagedObject>.init(entityName: "NewsList")
            fetchRequest.sortDescriptors = [sortDescriptor]
            
            var models: [TinkoffNewsTitleModel] = []
            if let fetchRequestResult = try? context.fetch(fetchRequest) {
                for coreDataModel in fetchRequestResult {
                    let newsID = coreDataModel.value(forKey: "newsID") as! Int64
                    let name = coreDataModel.value(forKey: "name") as! String
                    let text = coreDataModel.value(forKey: "text") as! String
                    let publicationTimestamp = coreDataModel.value(forKey: "publicationTimestamp") as! Int64
                    let typeID = coreDataModel.value(forKey: "typeID") as! Int16
                    
                    let model = TinkoffNewsTitleModel.init(with: newsID, name: name, text: text, publicationTimestamp: publicationTimestamp, typeID: typeID)
                    models.append(model)
                }
            }
            
            performBlockInMainQueue {
                completion(models)
            }
        }
    }
    
    private func coreDataSave(context: NSManagedObjectContext) {
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                print("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
}
