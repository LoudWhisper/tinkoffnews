//
//  TinkoffNewsNetworkService.swift
//  TinkoffNews
//
//  Created by Daniil Gavrilov on 26.01.17.
//  Copyright © 2017 Daniil Gavrilov. All rights reserved.
//

import UIKit

private struct Constants {
    struct Keys {
        static let id = "id"
        static let payload = "payload"
    }
    
    enum Addresses: String {
        case main = "https://api.tinkoff.ru"
    }
    
    enum Paths: String {
        case news = "/v1/news"
        case newsContent = "/v1/news_content"
    }
}

class TinkoffNewsNetworkService {
    
    typealias CompletionBlock = (_ response: Any?, _ errorMessage: String?, _ isNotReachable: Bool, _ isTimedOut: Bool) -> (Void)
    
    // MARK: Private Properties
    
    private var networkSession = URLSession.init(configuration: URLSessionConfiguration.default)
    
    // MARK: Public Methods
    
    func getNewsList(completion: CompletionBlock?) {
        let requestURL = self.requestURL(address: Constants.Addresses.main, path: Constants.Paths.news)
        let task = self.networkSession.dataTask(with: requestURL) { (data, response, error) in
            self.perform(completion: completion, data: data, response: response, error: error)
        }
        
        task.resume()
    }
    
    func getNewsDetail(newsID: Int64, completion: CompletionBlock?) {
        let parameters = [Constants.Keys.id : String(newsID)]
        let requestURL = self.requestURL(address: Constants.Addresses.main, path: Constants.Paths.newsContent, parameters: parameters)
        
        let task = self.networkSession.dataTask(with: requestURL) { (data, response, error) in
            self.perform(completion: completion, data: data, response: response, error: error)
        }

        task.resume()
    }
    
    // MARK: Private Methods
    
    // MARK: Build URL
    
    private func requestURL(address: Constants.Addresses, path: Constants.Paths) -> URL {
        return self.requestURL(address: address, path: path, parameters: nil)
    }
    
    private func requestURL(address: Constants.Addresses, path: Constants.Paths, parameters: [String:String]?) -> URL {
        let requestString = address.rawValue + path.rawValue
        var requestURL: URL?

        if parameters != nil {
            var queryItems: [URLQueryItem] = []
            for (key, value) in parameters! {
                let queryItem = URLQueryItem.init(name: key, value: value)
                queryItems.append(queryItem)
            }
            
            var components = URLComponents.init(string: requestString)
            components?.queryItems = queryItems
            
            requestURL = components?.url
        } else {
            requestURL = URL.init(string: requestString)
        }
        
        if (parameters != nil) {
            assert(requestURL != nil, "Request url can't be nil; info: {\naddress: \(requestString);\nparameters: \(parameters!) }")
        } else {
            assert(requestURL != nil, "Request url can't be nil; info: {\naddress: \(requestString) }")
        }
        
        return requestURL!
    }
    
    // MARK: Process Response
    
    private func perform(completion: CompletionBlock?, data: Data?, response: URLResponse?, error: Error?) {
        let isNotReachable = (data == nil) && (response == nil) && (error == nil)
        let isTimedOut = (response as? HTTPURLResponse)?.statusCode == 0
        let errorMessage = self.errorMessage(with: error, isNotReachable: isNotReachable, isTimedOut: isTimedOut)
        
        var payload: Any?
        if data != nil {
            let serializedData = try! JSONSerialization.jsonObject(with: data!, options: [])
            if let serializedDictionary = serializedData as? [String:Any] {
                payload = serializedDictionary[Constants.Keys.payload]
            }
        }
        
        completion?(payload, errorMessage, isNotReachable, isTimedOut)
    }
    
    private func errorMessage(with error: Error?, isNotReachable: Bool, isTimedOut: Bool) -> String? {
        var message: String?
        
        if isNotReachable || isTimedOut {
            message = "Интернет соединение недоступно. Пожалуйста, попробуйте позже."
        } else if error != nil {
            message = error!.localizedDescription
        }
        
        return message
    }
    
}
