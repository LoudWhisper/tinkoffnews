//
//  TinkoffNewsEmptyTableViewCell.swift
//  TinkoffNews
//
//  Created by Daniil Gavrilov on 29.01.17.
//  Copyright © 2017 Daniil Gavrilov. All rights reserved.
//

import UIKit
import TableKit

class TinkoffNewsEmptyTableViewCell: UITableViewCell, ConfigurableCell {
    
    enum State {
        case empty
        case loading
    }

    typealias T = State
    
    // MARK: Interface Builder Properties
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    // MARK: Init Methods & Superclass Overriders
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    // MARK: Protocols Implementation
    
    // MARK: ConfigurableCell
    
    static var estimatedHeight: CGFloat? {
        return 56.0
    }
    
    func configure(with state: T) {
        switch state {
        case .empty:
            self.titleLabel.text = "Нет записей"
            self.activityIndicator.stopAnimating()
            break
            
        case .loading:
            self.titleLabel.text = ""
            self.activityIndicator.startAnimating()
            break
        }
    }

}
