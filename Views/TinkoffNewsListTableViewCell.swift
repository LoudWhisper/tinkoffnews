//
//  TinkoffNewsListTableViewCell.swift
//  TinkoffNews
//
//  Created by Daniil Gavrilov on 28.01.17.
//  Copyright © 2017 Daniil Gavrilov. All rights reserved.
//

import UIKit
import TableKit

class TinkoffNewsListTableViewCell: UITableViewCell, ConfigurableCell {
    
    typealias T = TinkoffNewsTitleModel
    
    // MARK: Interface Builder Properties
    
    @IBOutlet weak var titleLabel: UILabel!
    
    // MARK: Init Methods & Superclass Overriders

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    // MARK: Protocols Implementation
    
    // MARK: ConfigurableCell
    
    static var estimatedHeight: CGFloat? {
        return 44.0
    }
    
    func configure(with model: T) {
        self.titleLabel.text = model.text
    }

}
